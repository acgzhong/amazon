### 前台搜索页
#### 符合免费送货条件 Eligible for Free Shipping
- 亚马逊发货超过 35 美元的订单免费送货
- Amazon Prime会员可以享受无限次免费两天送货服务，无需最低消费

#### 气候友好承诺 Climate Pledge Friendly
- 亚马逊气候友好承诺旨在通过明确的标签、严格的标准、多样化的产品和教育透明度，帮助消费者选择更可持续的产品，同时推动亚马逊自身和其供应链走向更环保、更可持续的未来

#### 部门 Department
- 选择符合产品的类目
- 问题：如何选择合适的类目

#### 客户评价 Customer Reviews
- 星级评价和积极的评价内容能作为强有力的社交证明，增强潜在买家的信任感，增加购买决策的可能性

#### 品牌 Brands
- 关键词下的品牌展示
- 问题：怎么样使品牌展示在这里面

#### 价格 Price
- 搜索页价格筛选

#### 优惠与折扣 Deals & Discounts
1. 所有折扣 All Discounts
- 优惠券 Coupon
- 标签价格 List Price

2. 今日特卖 Today's Deals
- Extra 30% off when you subscribe

#### 其它参数
- 需自己搜索产品关键词进入搜索页查看左侧列表